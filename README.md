# Deep Learning Exercises


## Installation

To install a new IPython kernel with pipenv do the following:

```
pipenv install ipykernel
pipenv shell
python -m ipykernel install --user --name=`basename $VIRTUAL_ENV`
```

You can then install other packages you want via pipenv. In the notebook change  to the respective kernel.
